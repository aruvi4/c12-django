from django.urls import path
from . import views
urlpatterns = [
    path('helper', views.find_words, name='find_words'),
    path('reset', views.reset_constraints, name='reset')
]