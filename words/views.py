from django.shortcuts import render

# Create your views here.

# We start off with a local list.
# TODO: use file handling to fetch words from a local file.

word_list = ['apple', 'orange', 'mango', 'banana']

def find_words(request):
    
    context = {}

    if request.method == 'POST':
        size = int(request.POST['size'])
        inclusions = request.POST['inclusions']
        exclusions = request.POST['exclusions']

        request.session['size'] = size
        request.session['inclusions'] = inclusions
        request.session['exclusions'] = exclusions

        words = [w for w in word_list
                 if len(w) == size
                 and all([ch in w for ch in inclusions])
                 and all([ch not in w for ch in exclusions])]
        if words:
            context['words'] = words
        else:
            context['error'] = 'No words match your query'
    return render(request, 'words/index.html', context)

def reset_constraints(request):
    del request.session['size']
    del request.session['inclusions']
    del request.session['exclusions']
    return render(request, 'words/index.html')