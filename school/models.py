from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length = 100)
    roll_no = models.CharField(max_length = 10)
    course = models.CharField(max_length = 30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} - {self.roll_no} - {self.course}'

class StudentMark(models.Model):
    subject = models.CharField(max_length = 100)
    score = models.IntegerField()
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return f'({self.student.roll_no}) - {self.subject} - {self.score}'
    
