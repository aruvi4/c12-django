from django import forms
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from .models import Student

'''
One-time setup
Create permissions if needed
Create groups for user
Assign correct permissions for each group
Decorate your view with @permission_required

To-do for every new registration
Create a User object
Get the correct group object based on business logic
Add the group object to the set of groups in the user object
Save the user object
Create a Student object
Associate the user object you just created with the new Student object
Save the Student object
'''

class StudentForm(forms.ModelForm):

    user_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField()
    class Meta:
        model = Student
        fields = ['name', 'roll_no', 'course']
    
    #https://docs.djangoproject.com/en/4.2/ref/forms/validation/
    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get('user_password')
        confirm_password = cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError('Passwords do not match')
        if len(password) < 8:
            raise forms.ValidationError('Password too short, should be 8 or more characters')
        if '2023' in password:
            raise forms.ValidationError('Password cannot contain 2023')
    def save(self):
        username = self.cleaned_data.get('name')
        password = self.cleaned_data.get('user_password')
        email = self.cleaned_data.get('email')
        student_group = Group.objects.get(name='student')
        user = User.objects.create_user(username=username, email=email, password=password)
        user.groups.add(student_group)
        user.save()
        student = super(StudentForm, self).save(commit=False)
        student.user = user
        student.save()
        self.save_m2m()
        return student
    

class TeacherForm(UserCreationForm):

    email = forms.EmailField()

    def save(self, commit=True):
        user = super(TeacherForm, self).save()
        teacher_group = Group.objects.get(name='teacher')
        user.groups.add(teacher_group)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            self.save_m2m()
        return user

class SpecialCharField(forms.CharField):
    def to_python(self, value):
        print("i am now in field to_python stage")
        return super().to_python(value)
    
    def validate(self, value):
        print("i am now in field validate stage")
        return super().validate(value)
        
    
    def clean(self, value):
        print("i am now in field clean stage")
        return super().clean(value)
        

class HelloForm(forms.Form):
    name = SpecialCharField()

    def clean_name(self):
        print("i am now in form-clean stage for individual fields")
        return self.cleaned_data["name"]
    
    def clean(self):
        super().clean()
        print("i am now in form-clean stage for the whole form")

    
    def save(self):
        print("i am now in form-save stage")
        return self.cleaned_data["name"]