from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User, Group
from django.http import HttpResponse, HttpResponseForbidden
from .models import Student, StudentMark
from .forms import StudentForm, TeacherForm, HelloForm
from django.urls import reverse
# Create your views here.

#R for READ
@permission_required('school.view_student', login_url='login')
def student_list(request):
    students = Student.objects.all()
    return render(request, 'school/student_list.html', 
                  {'students': students})


#C for CREATE
@permission_required('school.add_student', login_url='login')
def add_student(request):
    return redirect('student_register')

#R for READ    
@permission_required('school.view_studentmark', login_url='login')
def student_detail(request, id):
    s = Student.objects.get(id=id)
    teacher_group = Group.objects.get(name='teacher')
    if s.user != request.user and not teacher_group in request.user.groups.all():
        return render(request, 'school/login.html', {'error': 'You are not allowed to view this page'})
    else:
        marks = StudentMark.objects.filter(student=s)
        return render(request, 'school/student_detail.html',
                    {'student': s, 'marks': marks})

#D for DELETE
@permission_required('school.delete_student', login_url='login')
def delete_student(request, id):
    s = Student.objects.get(id=id)
    s.delete()
    return redirect('student_list')

#U for UPDATE
@permission_required('school.change_student', login_url='login')
def update_student(request, id):
    if request.method == 'GET':
        s = Student.objects.get(id=id)
        editing = True
        context = {'student': s, 'editing': editing}
        return render(request, 'school/student_detail.html', context)

    if request.method == 'POST':
        s = Student.objects.get(id=id)
        s.name = request.POST['name']
        s.course = request.POST['course']
        s.roll_no = request.POST['roll_no']
        s.save()
        return redirect('student_detail', id)

#C for CREATE    
@permission_required('school.add_studentmark',login_url='login')
def add_mark(request, id):
    s = Student.objects.get(id=id)
    new_subject = request.POST['subject']
    new_score = request.POST['score']
    s = StudentMark.objects.create(
            student=s,
            subject=new_subject,
            score=new_score
        )
    return redirect('student_detail', id)

#U for UPDATE and D for DELETE and R fo READ
@permission_required('school.change_student', login_url='login')
def edit_mark(request, id, mark_id):

    m = StudentMark.objects.get(id=mark_id)
    if request.method == 'GET':
        return render(request, 'school/marks.html', {'mark': m})
    elif request.method == 'POST':
        if request.POST.get('edit') == 'EDIT':
            m.subject = request.POST['subject']
            m.score = request.POST['score']
            m.save()
            return redirect('edit_mark', id, mark_id)
        else:
            m.delete()
            return redirect('student_details', id)
        
def student_register(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = StudentForm()
    return render(request, "school/register.html", {'form': form, 'action': reverse('student_register')})

def teacher_register(request):
    if request.method == 'POST':
        form = TeacherForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = TeacherForm()
    return render(request, "school/register.html", {'form': form, 'action': reverse('teacher_register')})
        
def login_view(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        u = authenticate(username=username, password=password)
        if u is not None:
            login(request, u)
            return redirect('student_list')
        else:
            error = 'Wrong username or password'
            context['error'] = error
    return render(request, 'school/login.html', context)
        
def logout_view(request):
    logout(request)
    return redirect('login')

def say_hello(request):
    if request.method == 'POST':
        form = HelloForm(request.POST)
        if form.is_valid():
            name = form.save()
        return HttpResponse(f'Hello {name}')
    else:
        form = HelloForm()
    return render(request, 'school/hello.html', {'form': form})