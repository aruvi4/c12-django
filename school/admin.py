from django.contrib import admin
from .models import Student, StudentMark
from django.contrib.auth.models import Permission

admin.site.register([Student, StudentMark, Permission])

# Register your models here.
